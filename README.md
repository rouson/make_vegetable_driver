# make_vegetable_driver

Tool for creating the driver program for a [vegetables] test suite.

Usage is as:

```
make_vegetable_driver driver_name test_file [more [test [files [...]]]]
```

## Installation

Build and install with [fpm].

[vegetables]: https://gitlab.com/everythingfunctional/vegetables
[fpm]: https://github.com/fortran-lang/fpm
